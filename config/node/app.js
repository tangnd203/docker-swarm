var http = require('http');
const { hostname } = require('os');


http.createServer(function(req, res) {
    res.writeHead(200,{'Context-Type':'text/plain'});
    res.end("Swarm service Node App running ... with hostname = "+hostname);

}).listen(8085);

console.log("Node App run 8085 with hostnam = " + hostname)