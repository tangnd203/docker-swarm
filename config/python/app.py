import http.server
import socket
import time

hostname = socket.gethostname()

class MyHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        time.sleep(1)
        self.send_response(200)
        self.send_header('Content-type', 'text/plain')
        self.end_headers()
        self.wfile.write(bytes("Swarm service Python App running ... with hostname = " + hostname, "utf-8"))

def run(server_class=http.server.HTTPServer, handler_class=MyHandler, port=8085):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print("Python App run 8085 with hostname = " + hostname)
    httpd.serve_forever()

if __name__ == "__main__":
    run()
